#!/usr/bin/php
<?php

require("email.php");

echo "Parsing Email...\n";

$options = getopt("f:");
$help="ERROR: Incorect parameters passed. (./parse.php -f email.txt)";
if($options['f']=='')
{
	echo "$help\n";
	exit();
}
#check if they used the -h parameter and print the help string.

#read the file specified in the command line parameters.
$filename = $options['f'];
$handle = fopen($filename, 'r') or die("Unable to open file.");

$email_text = "";

#read in the file line by line.
if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        $email_text .= $buffer;
    }
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    fclose($handle);
}

$email = new Email($email_text);

echo "From: ".$email->get_sender()."\n";
echo "To: ".$email->get_recipient()."\n";
echo "Date: ".$email->get_date()."\n";
echo "Subject: ".$email->get_subject()."\n";
echo "---\n";
echo "Message: ".$email->get_message()."\n";

?>

<?php
class Person {

	function __construct($person) {
		#$person is a string in the format:
		#Neil Monday <neilmonday@me.com>
		#OR just:
		#NeilMonday@gmail.com
	
		if (strpos($person,' ') !== false) {
			$person_parts = array();
			$person_parts = explode("<", $person, 2);
			$this->name = rtrim($person_parts[0]);
			$this->email = rtrim($person_parts[1], ">");
		}
		else
		{
			$this->name = "";
			$this->email = $person;
		}
	}
	
	function __toString() {
		return "Name: ".$this->name." Email: ".$this->email;
	}
	
	var $email;
	var $name;
}
?>

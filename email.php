<?php

require("person.php");
require("message.php");

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    $start  = $length * -1; //negative
    return (substr($haystack, $start) === $needle);
}

function splitBoundaries($text)
{

}

class Email {
	
	protected $subject;					#String "Subject:"
	protected $sender;					#Person "From:"
	protected $recipient;				#Person "To:"
	protected $date;					#?? "Date:"
	protected $message; # = array();		#Message; should be an array of messages to accommodate emails with html and plain text
	
	#I guess I could just put parse() function's contents as the constructor. This allows me to re-parse though.
	public function parse($text) {
	
		#WRONG! Do not split on \n\n\n. Gotta split on the --Boundary[…] instead.
		#$email_sections = explode("\n\n\n", $text); #\n\n only means 1 blank line. I am looking for 2.

		$email_sections =  explode("\n\n", $text, 2);		
		$email_headers = explode("\n", $email_sections[0]);
		$email_message = $email_sections[1]; //could be multiple messages. Maybe check sizeof email_sections?

		//parse the headers
		foreach($email_headers as $line)
		{
			$line_array = array();
			$parts_array = array();
			$title = "";
			
			#Read header fields
			if($line == ltrim($line))
			{
				#echo $line."\n";
				$line_array = explode(":", $line, 2); #only split the first occurrence of :
				$title = $line_array[0];
				if(sizeof($line_array) > 1)
				{	
					$parts_array[] = ltrim($line_array[1]);
				}
			}
			else
			{
				$line = ltrim($line);
				array_push($parts_array, $line);
			}
			
			if(!empty($title))
			{
				#echo "TITLE: ".$title."\n";
				if($title == "From")
					$this->sender = new Person(implode($parts_array));
				else if($title == "To")
					$this->recipient = new Person(implode($parts_array));
				else if($title == "Subject")
					$this->subject = $parts_array[0];
				else if($title == "Date")
					$this->date = $parts_array[0];					
			}
		}

		$this->message = new Message($email_message);
	}
	

	function __construct($text) {
		$this->parse($text);
	}
	
	public function get_sender(){return $this->sender;}
	public function get_recipient(){return $this->recipient;}
	public function get_subject(){return $this->subject;}
	public function get_date(){return $this->date;}
        public function get_message(){return $this->message;}
}

?>

<?php
class Message {
    public function __construct($email_message)
    {
        $temp_message = "";
        $email_message = ltrim($email_message);
        $pos = strpos($email_message, "--");

        #echo $email_message."\n";
	#echo "POSITION: ".$pos."\n";
        #echo "POSITION IS NULL? ".is_null($pos)."\n";
        #echo "POSITION IS EMPTY? ".empty($pos)."\n";
        if($pos === 0)//we need to look out for boundaries and more headers
        {
            //split $email_message into lines,
            //iterate through lines,
            //append all lines after the first "\n\n" to the $temp_message

            $record = false;
            $lines = explode("\n",$email_message);
            foreach($lines as $line)
            {
                if($line == "")
                    $record = true;

                if($record == true)
                {
                    if(startsWith($line, "--"))
                        break;

                    $temp_message .= $line."\n";
                }
            }
        }
        else //we are good to go?
        {
            $temp_message = $email_message;
        }

	#there is probably a better way to do this.
	$temp_message = implode(explode("=\n", $temp_message));
	$this->body = $temp_message;
    }

    public function __toString()
    {
        return $this->body;
    }

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    $start  = $length * -1; //negative
    return (substr($haystack, $start) === $needle);
}


    private $content_type;
    private $transfer_encoding;
    private $body;
}
?>
